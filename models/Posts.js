const { Schema, model } = require ("mongoose");

const Posts = new Schema({
    header: {
        type: String,
        required: true,
        minlength:3,
        maxlength:50
    },
    image: {
        type: String,
        minlength:3,
        maxlength:50
    },
    content: {
        type: String,
        required: true,
        minlength:3,
        maxlength:200
    },
    link: { type: String },
    isActive: { type: Boolean, default: false }
});

module.exports = model('Post', Posts)