const { Schema, model } = require ("mongoose");

const Users = new Schema({
    login: {
        type: String,
        uniqued: true,
        required: true,
        minlength:3,
        maxlength:20
    },
    password: {
        type: String,
        required: true,
        minlength:3,
        maxlength:20
    },
    email: {
        type: String,
        uniqued: true,
        required: true,
        minlength:3,
        maxlength:20
    },
    fullname: {
        type: String,
        minlength:3,
        maxlength:20
    },
    link: { type: String },
    isActive: { type: Boolean, default: false }
});

module.exports = model('User', Users)