const PORT = 9001

const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const {secret} = require("./config")
const URL_DB = "mongodb+srv://admin:1334@cluster0.csdgt.mongodb.net/?retryWrites=true&w=majority"
const Users = require('./models/Users')
const Posts = require('./models/Posts')


const app = express()

const generateAccessToken = (id) => {
    const payload = {
        id
    }
    return jwt.sign(payload, secret, {expiresIn : '24h'})
}

// указание статичной папки
app.use(express.static(__dirname + "/static"))
app.use(cors())
app.use(express.json())

app.post('/addnews', async (req, res) => {
    console.log(req.body)
    const {header, image, content} = req.body
    const news = new Posts({header, image, content})
    await news.save()
    res.json({
        header: header,
        image: image,
        content: content
    })
})

app.get('/new', async (req, res) =>{
    const results = await Posts.find();
    res.json(results);
})

app.delete('/deletepost', async (req, res) =>{
    const post_id = req.body.id
    /*Posts.deleteOne({_id: post_id})*/
    const result = await Posts.deleteOne({_id : post_id})
    console.log(post_id)
})

app.post('/registration', async (req, res) => {
    console.log(req.body)
    const {login, password, email} = req.body
    let condidate = await Users.findOne({login})
    if (condidate){
        return res.status(400).json({message : "Данный пользователь уже зарегистрирован"})
    }
    const user = new Users({login, password, email})
    await user.save()
    res.json({
        login: login,
        password: password,
        email: email
    })
})
app.post('/login', async (req, res) => {
    console.log(req.body)
    const {login, password} = req.body
    let condidate = await Users.findOne({login})
    if (!condidate){
        return res.status(400).json({message : "Данный пользователь уже зарегистрирован"})
    }
    if(condidate.password !== password){
        return res.status(400).json({message : "Неверный логин или пароль"})
    }
    const token = generateAccessToken(condidate._id)
    res.json({
        login: login,
        password: password,
        token: token
    })
})
app.get('/page', (req, res) => {
    res.send('<h1>Страница новостей</h1>')
})
app.get('/', (req, res) => {
    res.sendFile('index.html')
})


const start = async () => {
    try {
        await mongoose.connect(URL_DB, { useNewUrlParser: true, useUnifiedTopology: true, authSource:"admin" })
        app.listen(PORT, () => console.log(`Сервер запущен на ${PORT} порте`))
    } catch (e) {
        console.log(e)
    }
}

start()